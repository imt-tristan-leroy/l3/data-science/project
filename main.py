import json
import os.path
import pickle

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.neighbors import KNeighborsClassifier
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import train_test_split, cross_val_score, cross_val_predict
from sklearn.linear_model import LogisticRegression, Perceptron
from sklearn import metrics
import seaborn as sns


def read_data_from_csv(path):
    """
    Read the data from a csv file
    :param path: path to a csv file
    :type path: str
    :returns: The data parsed into a dataframe object
    :rtype: pd.DataFrame
    """
    df = pd.read_csv(
        path,
        header=0,
        delimiter=",",
        dtype={
            "fixed acidity": np.float64,
            "volatile acidity": np.float64,
            "citric acid": np.float64,
            "residual sugar": np.float64,
            "chlorides": np.float64,
            "free sulfur dioxide": np.float64,
            "total sulfur dioxide": np.float64,
            "density": np.float64,
            "pH": np.float64,
            "sulphates": np.float64,
            "alcohol": np.float64,
            "quality": np.int32
        }
    )
    return df


def save_dataframe_to_csv(df, folder="", name="red_wines"):
    """
    Display dataframe information like NA field, or histogram of the data
    :param df: The dataframe to display
    :type df: pd.DataFrame
    :param folder: Folder to put the exported file
    :type folder: str
    :param name: Name of the exported file
    :type name: str
    """
    if not os.path.exists(folder):
        os.makedirs(folder)

    # Display The dataframe
    df.to_csv(f"{folder}/{name}.csv")


def save_dataframe_information(df, folder=""):
    """
    Save dataframe information like NA field, or histogram of the data
    :param folder: Folder to put the exported file
    :type folder: str
    :param df: The dataframe to display
    :type df: pd.DataFrame
    """
    save_dataframe_to_csv(df, folder)

    # Get the na dataframe and save it
    df_is_na = df.isna()
    save_dataframe_to_csv(df_is_na, folder, "red_wines_is_na")

    # Display the column where the data is missing
    save_dataframe_to_csv(df_is_na.sum(), folder, "red_wines_is_na_sum")

    if not os.path.exists(folder):
        os.makedirs(folder)

    # Get the correlation dataframe and save it
    df_corr = df.corr()
    df_corr.to_csv(f"{folder}/red_wines_corr.csv")

    for column in df:
        fig, ax = plt.subplots()
        df.hist(column, ax=ax)

        if not os.path.exists(f'{folder}/hist'):
            os.makedirs(f'{folder}/hist')

        fig.savefig(f'{folder}/hist/{column}.png')
        plt.close(fig)

    pd.plotting.scatter_matrix(df, alpha=0.2, figsize=(25, 25), diagonal='kde')
    plt.savefig(f'{folder}/scatter_matrix.png')
    plt.close()


def correct_data(df):
    """
    Fixe the value in the dataframe by filling the NA value and replace
    :param df: The dataframe to correct
    :type df: pd.DataFrame
    :return: The dataframe with corrected value
    :rtype: pd.DataFrame
    """

    # Remove the ph value over 14
    df.loc[(df['pH'].values > 14), 'pH'] = np.NaN

    # Fill the missing data with the data mean in the column
    df = df.fillna(df.mean())

    return df


def normalize_data(df):
    """
    Normalize the dataframe
    :param df: The dataframe to normalize
    :type df: pd.DataFrame
    :return: The dataframe with normalized value
    :rtype: pd.DataFrame
    """
    scaler = StandardScaler()
    scaler.fit(df)
    scaled = scaler.transform(df)
    scaled_df = pd.DataFrame(scaled, columns=df.columns)
    scaled_df['quality'] = df['quality']
    return scaled_df


def remove_useless_columns(df):
    """
    Remove the useless columns in this analyse
    :param df: The dataframe to remove columns
    :type df: pd.DataFrame
    :return: The dataframe with corrected value
    :rtype: pd.DataFrame
    """
    df.drop(columns=["fixed acidity", "residual sugar", "free sulfur dioxide", "pH"], inplace=True)
    return df


def split_data(df, test_size=0.2):
    """
    Split the data into two set the first is for training and the second is for testing
    :param df: The dataframe to split
    :type df: pd.DataFrame
    :param test_size: Size for the test set
    :type test_size: float
    :return: Tuple composed of a train and a test set and result
    :rtype: (pd.DataFrame, pd.DataFrame, pd.DataFrame, pd.DataFrame)
    """
    feature_columns = list(df)
    feature_columns.remove('quality')
    x = df[feature_columns]
    y = df['quality']
    x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=test_size)
    return x_train, y_train, x_test, y_test


def fit_model(model, x_set, y_set):
    """
    Train the model with the set in params
    :param model: The model used to training
    :type model: LogisticRegression | Perceptron | KNeighborsClassifier
    :param x_set: The set used for training
    :type x_set: pd.DataFrame
    :param y_set: The result of the set
    :type y_set: pd.DataFrame
    :return: The trained model
    :rtype: LogisticRegression | Perceptron | KNeighborsClassifier
    """
    model.fit(x_set, y_set)
    return model


def evaluate_model(model, x_set, y_set, cv=5):
    """
    Evaluate the model with the set passed in params
    :param model: The model using to test the set
    :type model: LogisticRegression | Perceptron | KNeighborsClassifier
    :param x_set: The set to test
    :type x_set: pd.DataFrame
    :param y_set: The result of the set
    :type y_set: pd.DataFrame
    :param cv: cross validation strategy
    :type cv: int
    :return: The different scores calculated
    :rtype: np.ndarray, np.ndarray, np.ndarray, np.ndarray, np.ndarray
    """
    # Calculate a prediction for the confusion matrix
    y_pred = cross_val_predict(model, x_set, y_set, cv=cv)
    confusion_matrix = metrics.confusion_matrix(y_set, y_pred)

    # Evaluate the different scores
    accuracy_score = cross_val_score(model, x_set, y_set, cv=cv, scoring="accuracy")
    precision_score = cross_val_score(model, x_set, y_set, cv=cv, scoring="precision")
    recall_score = cross_val_score(model, x_set, y_set, cv=cv, scoring="recall")
    f1_score = cross_val_score(model, x_set, y_set, cv=cv, scoring="f1")

    return confusion_matrix, accuracy_score, precision_score, recall_score, f1_score


def save_evaluation(confusion_matrix, accuracy_score, precision_score, recall_score, f1_score, folder="evaluate"):
    """
    Save the evaluation in files
    :param confusion_matrix: Confusion matrix
    :type confusion_matrix: np.ndarray
    :param accuracy_score: Accuracy score array
    :type accuracy_score: np.ndarray
    :param precision_score: Precision score array
    :type precision_score: np.ndarray
    :param recall_score: Recall score array
    :type recall_score: np.ndarray
    :param f1_score: F1 score array
    :type f1_score: np.ndarray
    :param folder: The folder where the model will be saved
    :type folder: str
    """
    if not os.path.exists(folder):
        os.makedirs(folder)

    # Create a heat map figure for confusion matrix
    fig, ax = plt.subplots()
    sns.heatmap(confusion_matrix, annot=True, cmap="YlGnBu", fmt='g', ax=ax)

    # labels, title and ticks
    ax.set_xlabel('Predicted labels')
    ax.set_ylabel('True labels')
    ax.set_title('Confusion Matrix')
    ax.xaxis.set_ticklabels(['-1', '1'])
    ax.yaxis.set_ticklabels(['-1', '1'])

    fig.savefig(f'{folder}/confusion_matrix.png')
    plt.close(fig)

    # Write evaluation in a json file
    json_data = {
        'confusion_matrix': confusion_matrix.tolist(),
        'accuracy_score': {
            'scores': accuracy_score.tolist(),
            'means': np.mean(accuracy_score)
        },
        'precision_score': {
            'scores': precision_score.tolist(),
            'means': np.mean(precision_score)
        },
        'recall_score': {
            'scores': recall_score.tolist(),
            'means': np.mean(recall_score)
        },
        'f1_score': {
            'scores': f1_score.tolist(),
            'means': np.mean(f1_score)
        },
    }
    with open(f'{folder}/evaluate_result.json', 'w') as outfile:
        json.dump(json_data, outfile, indent=4)
        outfile.close()

    # print("Confusion:", confusion_matrix)
    # print("Accuracy:", accuracy_score)
    # print("Precision:", precision_score)
    # print("Recall:", recall_score)
    # print("F1:", f1_score)


def save_model(model, folder="model"):
    """
    Save the models in files
    :param model: The model to save
    :type model: LogisticRegression | Perceptron | KNeighborsClassifier
    :param folder: The folder where the model will be saved
    :type folder: str
    """
    if not os.path.exists(folder):
        os.makedirs(folder)

    pickle.dump(model, open(f"{folder}/model.sav", 'wb'))


def load_model(path="model"):
    """
    Load a models from file
    :param path: Path to the model
    :type path: str
    :return: The loaded models
    :rtype: LogisticRegression | Perceptron | KNeighborsClassifier
    """

    model = pickle.load(open(path, 'rb'))
    return model


def main():
    df = read_data_from_csv("red_wines.csv")
    # Display the different dataframe information before correction
    save_dataframe_information(df, "before-correction")

    df = correct_data(df)
    # Display the different dataframe information before correction
    save_dataframe_information(df, "after-correction")

    df = normalize_data(df)
    # Display the different dataframe information after normalizing
    save_dataframe_information(df, "after-normalizing")

    df = remove_useless_columns(df)
    # Display the different dataframe information after removing columns
    save_dataframe_information(df, "after-columns-removing")

    x_train, y_train, x_test, y_test = split_data(df)
    # Save the train set
    save_dataframe_to_csv(x_train, "train-set/x")
    # Display the different dataframe information for the train set
    save_dataframe_to_csv(y_train, "train-set/y")
    # Display the different dataframe information for the test set
    save_dataframe_to_csv(x_test, "test-set/x")
    # Display the different dataframe information for the test set
    save_dataframe_to_csv(y_test, "test-set/y")

    models = [
        (LogisticRegression(), "logistic-regression"),
        (Perceptron(), "perceptron"),
        (KNeighborsClassifier(), "k-neighbors-classifier")
    ]

    for model in models:
        # Train the model
        m = fit_model(model[0], x_train, y_train)

        # Get the evaluation data
        confusion_matrix, accuracy_score, precision_score, recall_score, f1_score = evaluate_model(
            m, x_test, y_test, 10)

        # Save evaluation data into a correct file
        save_evaluation(confusion_matrix, accuracy_score, precision_score, recall_score, f1_score,
                        f"evaluate/{model[1]}")

        # Save the model trained
        save_model(m, f"model/{model[1]}")

    # Load model and get prediction
    m = load_model("model/perceptron/model.sav")
    predict = m.predict(x_test)
    print(predict)


if __name__ == '__main__':
    main()
